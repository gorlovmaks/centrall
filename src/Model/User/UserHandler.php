<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/11/18
 * Time: 2:23 PM
 */

namespace App\Model\User;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Firebase\JWT\JWT;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(
        EntityManagerInterface $entityManager,
        ContainerInterface $container
    )
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    public function createNew(
        string $organizationName,
        string $organizationSiteURL)
    {
        $user = new User;
        $user->setOrganizationName($organizationName);
        $user->setOrganizationSiteURL($organizationSiteURL);
        $user->setToken('temporary' . uniqid());
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $token = array(
            "organizationName" => $organizationName,
            "organizationSiteURL" => $organizationSiteURL,
            "id" => $user->getId()
        );

        $jwt = JWT::encode(
            $token,
            $this->container->getParameter('secret')
        );

        $user->setToken($jwt);
        $this->entityManager->flush();
        return $user;
    }
}
