<?php


namespace App\Model\Client;

use App\Entity\Client;

class ClientHandler
{
    /**
     * @param array $data
     * @return Client
     */
    public function createNewClient(array $data) {
        $client = new Client();
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $password = md5($data['password']).md5($data['password'].'2');
        $client->setPassword($password);

        return $client;
    }
}
