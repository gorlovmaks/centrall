<?php

namespace App\DataFixtures;


use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class ClientFixtures extends Fixture
{

    /**
     * @var ClientHandler
     */
    private $clientHandler;

    public function __construct(ClientHandler $clientHandler)
    {
        $this->clientHandler = $clientHandler;

    }

    public function load(ObjectManager $manager)
    {

        $client = $this->clientHandler->createNewClient([
            'email' => '123@123.ru',
            'passport' => 'some & passport',
            'password' => 'pass_1234',
        ]);

        $manager->persist($client);
        $manager->flush();
    }
}
