<?php

namespace App\Security;

use App\Security\ApiKeyUserProvider;
use App\Security\PreAuthenticatedToken\AdminPreAuthenticatedToken;
use App\Security\PreAuthenticatedToken\AnonymousPreAuthenticatedToken;
use App\Security\PreAuthenticatedToken\UserPreAuthenticatedToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;

class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface
{
    public const USER_API_KEY_PARAMETER = 'token';

    public function createToken(Request $request, $providerKey)
    {
        // look for an api key query parameters
        $userApiKey = $request
            ->query
            ->get(
                self::USER_API_KEY_PARAMETER,
                $request
                    ->request
                    ->get(self::USER_API_KEY_PARAMETER)
            );


        if ($userApiKey) {
            return new UserPreAuthenticatedToken('anon.', $userApiKey, $providerKey, array('ROLE_USER'));
        }

        return new AnonymousPreAuthenticatedToken($providerKey);
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return (
                $token instanceof UserPreAuthenticatedToken
                || $token instanceof AnonymousPreAuthenticatedToken
            )
            && $token->getProviderKey() === $providerKey;
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if ($token instanceof AnonymousPreAuthenticatedToken) {
            return $token;
        }

        if (!$userProvider instanceof ApiKeyUserProvider) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The user provider must be an instance of ApiKeyUserProvider (%s was given).',
                    get_class($userProvider)
                )
            );
        }

        $apiKey = $token->getCredentials();

        if ($token instanceof UserPreAuthenticatedToken) {
            return $this->authenticateUserToken($userProvider, $apiKey, $providerKey);
        } else {
            throw new \InvalidArgumentException(
                sprintf(
                    'The token must be an instance of UserPreAuthenticatedToken || AnonymousPreAuthenticatedToken (%s was given).',
                    get_class($token)
                )
            );
        }
    }

    private function authenticateUserToken(ApiKeyUserProvider $userProvider, string $apiKey, string $providerKey)
    {
        $user = $userProvider->getUserByToken($apiKey);

        if (!$user) {
            // CAUTION: this message will be returned to the client
            // (so don't put any un-trusted messages / error strings here)
            throw new CustomUserMessageAuthenticationException(
                sprintf('Token "%s" does not exist.', $apiKey)
            );
        }

        return new UserPreAuthenticatedToken($user, $apiKey, $providerKey, array('ROLE_USER'));
    }
}
